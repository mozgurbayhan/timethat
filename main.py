# -*- coding: utf-8 -*-
import time

from timethat import TimedFunction, TimeThat

__author__ = 'ozgur'
__creation_date__ = '7.12.2019' '00:29'


@TimedFunction(talk=False)
def testfunction():
    time.sleep(1)


if __name__ == '__main__':
    testfunction()
    print(TimeThat.timedict["testfunction"]["cost"])
