init:
	pip install -r requirements.txt

tests:
	coverage run tests/test_timeme.py
	coverage report --show-missing

lint:
	pylint timethat.py

release:
	rm dist/*
	rm -rf timethat.egg-info
	python3 setup.py sdist
	twine check dist/*
	twine upload dist/*

.PHONY: init tests lint release
